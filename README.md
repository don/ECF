## Project Description

- This project aims to provide a registration app for company organizing career and educational fairs for mostly high school students.
- Contains landing page for five different venues, leading to registration page after picking up a specific venue.
- User side providing atendees with possibility to create their own itinerary for the fair, modify it and see a highlighted exhibitor's booths on the map.
- Admin part made for easy administration of exhibitors and statistics overview.

## Tech stack

- React and React Router for the front end
- Express server
- PostgreSQL database

## Scripts

- to run the app use npm start