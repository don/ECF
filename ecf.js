const { spawn } = require('child_process');

const app = spawn('npm', ['run', 'prod']);

app.stdout.on('data', (data) => {
  console.log(data.toString());
});

app.stderr.on('data', (data) => {
  console.log(data.toString());
});

app.on('close', (code) => {
  console.log(`Child process exited with code ${code}`);
});
