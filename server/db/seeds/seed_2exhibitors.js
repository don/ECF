
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('exhibitors').del()
    .then(function () {
      return Promise.all([
        // sponsors
        knex('exhibitors').insert({name: 'Sun Life Financial', description:'Need some advice about planning? We get it, and we’re here to help.', url_link:'https://www.sunlife.ca', sponsor: 1, video_link: 'gWBRxNbkETU', workshop: 0, seminar: 0, image: 'https://cdn.sunlife.com/static/canada/images/logo_yellowbkgrd_bluelogo_1200x1200.jpg'}),
        knex('exhibitors').insert({name: 'BC Auto Careers', description:'The B.C. Automotive Sector Alliance (BCASA) is a partnership between the Automotive Retailers Association (ARA) and the New Car Dealers Association (NCDA) of BC.', url_link:'http://www.bcautocareers.ca', sponsor: 1, video_link: 'ETLXZ_3sAiQ', workshop: 0, seminar: 0, image: 'http://www.ara.bc.ca/wp-content/uploads/BCASA_Logo.jpg'}),
        knex('exhibitors').insert({name: 'RCMP', description:'The RCMP offers an exceptional career, letting you make a real difference in your community and your country.', url_link:'http://www.rcmp-grc.gc.ca/en', sponsor: 1, video_link: '0eYQFOV-fBE', workshop: 0, seminar: 0, image: 'https://pbs.twimg.com/profile_images/590883724729962496/gcZOgG6E.jpg'}),
        knex('exhibitors').insert({name: 'Scotiabank', description:'Scotiabank is Canada’s international bank and a leading financial services provider in North America.', url_link:'https://www.scotiabank.com', sponsor: 1, video_link: 'wl3x8vmdTqg', workshop: 0, seminar: 0, image: 'http://www.downtownkingston.ca/sites/downtownkingston.ca/files/img/business/scotiabank.jpg'}),
        knex('exhibitors').insert({name: 'Canadian Armed Forces', description:'JOIN THE FORCES AS A DOCTOR AND RECEIVE A SIGNING BONUS', url_link:'http://www.forces.ca/en/home', sponsor: 1, video_link: 'fLwJ1SZgz2k', workshop: 0, seminar: 0, image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9b/Canadian_Forces_emblem.svg/200px-Canadian_Forces_emblem.svg.png'}),
        // regular exhibitors
        knex('exhibitors').insert({name: 'WorkBC Find Your Fit', description:'he Find Your Fit tour is a fun, interactive event designed to help students discover B.C. careers.', url_link:'https://www.workbc.ca/Jobs-Careers/Find-Your-Fit-Tour.aspx', sponsor: 0, video_link: '', workshop: 0, seminar: 0}),
        knex('exhibitors').insert({name: 'VanArts (Vancouver Institute of Media Arts)', description:'For over 20 years VanArts has been the launch point for new artistic and creative talent.', url_link:'https://www.vanarts.com', sponsor: 0, video_link: '', workshop: 0, seminar: 0}),
        knex('exhibitors').insert({name: 'Canadian Tourism College', description:'CTC’s Travel & Tourism programs give you a diverse set of skills', url_link:'www.tourismcollege.com', sponsor: 0, video_link: '', workshop: 0, seminar: 0}),
        knex('exhibitors').insert({name: 'Vancouver Film School', description:'Our alumni are shaping the industry, having worked on the top entertainment properties of 2016 - totaling more than $19 billion.', url_link:'https://vfs.edu', sponsor: 0, video_link: '', workshop: 0, seminar: 0}),
        knex('exhibitors').insert({name: 'Langara College', description:'Today, Langara is one of BC\'s leading undergraduate institutions providing University Studies', url_link:'http://langara.ca', sponsor: 0, video_link: '', workshop: 0, seminar: 0}),
        knex('exhibitors').insert({name: 'Okanagan School of Business/Commercial Aviation', description:'The Commercial Aviation diploma program is for men and women.', url_link:'http://www.okanagan.bc.ca/programs/areas_of_study/business/business_programs/commercial_aviation_diploma.html', sponsor: 0, video_link: '', workshop: 0, seminar: 0}),
        knex('exhibitors').insert({name: 'CG Masters School of 3D Animation & VFX', description:'EMPOWERING DIGITAL ARTISTS SINCE 2012', url_link:'http://cg-masters.com', sponsor: 0, video_link: '', workshop: 0, seminar: 0}),
        knex('exhibitors').insert({name: 'Pacific Institute of Culinary Arts', description:'Our intense, 90% hands-on training programs are delivered under close, demanding supervision', url_link:'https://www.picachef.com/', sponsor: 0, video_link: '', workshop: 0, seminar: 0}),
        // workshop
        knex('exhibitors').insert({name: 'Pacific Institute of Culinary Arts', description:'how to cook salmon', url_link:'', sponsor: 0, video_link: '', workshop: 1, seminar: 0}),
        // seminar
        knex('exhibitors').insert({name: 'RCMP', description:'about RCMP', url_link:'', sponsor: 1, video_link: '', workshop: 0, seminar: 1}),

      ]);
    });
};
