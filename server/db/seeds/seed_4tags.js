
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('tags').del()
    .then(function () {
      return Promise.all([
        // Inserts seed entries
        knex('tags').insert({tag: 'banking'}),
        knex('tags').insert({tag: 'government'}),
        knex('tags').insert({tag: 'financial'}),
        knex('tags').insert({tag: 'fine arts'}),
        knex('tags').insert({tag: 'culinary arts'}),
        knex('tags').insert({tag: 'college'}),
        knex('tags').insert({tag: 'collection'}),
        knex('tags').insert({tag: 'university'}),
        knex('tags').insert({tag: 'universal'}),
        knex('tags').insert({tag: 'media arts'}),
        knex('tags').insert({tag: 'medical'}),
      ]);
    });
};
