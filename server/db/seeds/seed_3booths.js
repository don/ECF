
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('booths').del()
    .then(function () {
      return Promise.all([
        // sponsors venue 1
        knex('booths').insert({ exhibitor_id : 1, venue_id : 1}),
        knex('booths').insert({ exhibitor_id : 2, venue_id : 1}),
        knex('booths').insert({ exhibitor_id : 3, venue_id : 1}),
       //exhibitor venue 1
        knex('booths').insert({ exhibitor_id : 6, venue_id : 1}),
        knex('booths').insert({ exhibitor_id : 7, venue_id : 1}),
        knex('booths').insert({ exhibitor_id : 8, venue_id : 1}),
        knex('booths').insert({ exhibitor_id : 9, venue_id : 1}),
        knex('booths').insert({ exhibitor_id : 10, venue_id : 1}),
        // workshop
        knex('booths').insert({ exhibitor_id : 14, venue_id : 1}),
        // seminar

        knex('booths').insert({ exhibitor_id : 15, venue_id : 1}),

        // sponsors venue 2
        knex('booths').insert({ exhibitor_id : 1, venue_id : 2}),
        knex('booths').insert({ exhibitor_id : 4, venue_id : 2}),
        knex('booths').insert({ exhibitor_id : 5, venue_id : 2}),
        //exhibitor venue 2
        knex('booths').insert({ exhibitor_id : 11, venue_id : 2}),
        knex('booths').insert({ exhibitor_id : 12, venue_id : 2}),
        knex('booths').insert({ exhibitor_id : 13, venue_id : 2}),
        knex('booths').insert({ exhibitor_id : 6, venue_id : 2}),
        knex('booths').insert({ exhibitor_id : 7, venue_id : 2}),

        knex('booths').insert({ exhibitor_id : 14, venue_id : 2}),

        // sponsors venue 3
        knex('booths').insert({ exhibitor_id : 2, venue_id : 3}),
        knex('booths').insert({ exhibitor_id : 3, venue_id : 3}),
        knex('booths').insert({ exhibitor_id : 4, venue_id : 3}),
        //exhibitor venue 3
        knex('booths').insert({ exhibitor_id : 8, venue_id : 3}),
        knex('booths').insert({ exhibitor_id : 9, venue_id : 3}),
        knex('booths').insert({ exhibitor_id : 10, venue_id : 3}),
        knex('booths').insert({ exhibitor_id : 11, venue_id : 3}),
        knex('booths').insert({ exhibitor_id : 12, venue_id : 3}),

        knex('booths').insert({ exhibitor_id : 14, venue_id : 3}),
        // sponsors venue 4
        knex('booths').insert({ exhibitor_id : 3, venue_id : 4, location : '((346, 143),(27, 19))'}),
        knex('booths').insert({ exhibitor_id : 4, venue_id : 4, location : '((376, 143),(27, 19))'}),
        knex('booths').insert({ exhibitor_id : 5, venue_id : 4, location : '((537, 143),(27, 19))'}),
        //exhibitor venue 4
        knex('booths').insert({ exhibitor_id : 13, venue_id : 4, location : '((180, 143),(27, 19))'}),
        knex('booths').insert({ exhibitor_id : 6, venue_id : 4, location : '((207, 143),(27, 19))'}),
        knex('booths').insert({ exhibitor_id : 7, venue_id : 4, location : '((237, 143),(27, 19))'}),
        knex('booths').insert({ exhibitor_id : 8, venue_id : 4, location : '((266, 143),(27, 19))'}),
        knex('booths').insert({ exhibitor_id : 9, venue_id : 4, location : '((294, 143),(27, 19))'}),

        knex('booths').insert({ exhibitor_id : 14, venue_id : 4, location : '((105, 183),(89, 82))'}),
        // sponsors venue 5
        knex('booths').insert({ exhibitor_id : 2, venue_id : 5}),
        knex('booths').insert({ exhibitor_id : 3, venue_id : 5}),
        knex('booths').insert({ exhibitor_id : 4, venue_id : 5}),
        //exhibitor venue 5
        knex('booths').insert({ exhibitor_id : 10, venue_id : 5}),
        knex('booths').insert({ exhibitor_id : 11, venue_id : 5}),
        knex('booths').insert({ exhibitor_id : 12, venue_id : 5}),
        knex('booths').insert({ exhibitor_id : 13, venue_id : 5}),
        knex('booths').insert({ exhibitor_id : 6, venue_id : 5}),

        knex('booths').insert({ exhibitor_id : 14, venue_id : 5}),



      ]);
    });
};
