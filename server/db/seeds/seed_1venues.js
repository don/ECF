
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('venues').del()
    .then(function () {
      return Promise.all([
        // Inserts seed entries
        knex('venues').insert({id: 1, name: 'Nanaimo', contact_info: '', video_link: 'MCMU-_TVkEA', opening_date: '2017-11-23' ,venue_map: '/maps/1.jpg' }),
        knex('venues').insert({id: 2, name: 'Surrey', contact_info: '', video_link: 'wRhQLLNXQ7U', opening_date: '2017-11-28', venue_map: '/maps/2.jpg' }),
        knex('venues').insert({id: 3, name: 'Vancouver', contact_info: '', video_link: 'OiJsbXXq-AY', opening_date: '2017-11-30', venue_map: '/maps/3.jpg'}),
        knex('venues').insert({id: 4, name: 'Abbotsford', contact_info: '', video_link: 'yKwHmdkAbb8', opening_date: '2017-12-05', venue_map: '/maps/4.jpg' }),
        knex('venues').insert({id: 5, name: 'Kelowna', contact_info: '', video_link: 'uhN9uRoqDEA', opening_date: '2017-12-07', venue_map: '/maps/5.jpg' }),

      ]);
    });
};
