
exports.up = function(knex, Promise) {
   return knex.schema.createTable('exhibitors', function (table) {
    table.increments();
    table.string('name');
    table.string('description');
    table.string('url_link');
    table.string('video_link');
    table.boolean('sponsor');
    table.boolean('workshop');
    table.boolean('seminar');

  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('exhibitors');
};
