
exports.up = function(knex, Promise) {
  return knex.schema.alterTable('itineraries', function(table) {
    table.integer('user_id').notNullable().alter();
    table.integer('exhibitor_id').notNullable().alter();
    table.integer('venue_id').notNullable().alter();
    table.unique(['user_id', 'exhibitor_id', 'venue_id']);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('itineraries', function (table) {
    table.integer('user_id').nullable().alter();
    table.integer('exhibitor_id').nullable().alter();
    table.integer('venue_id').nullable().alter();
    table.dropUnique(['user_id', 'exhibitor_id', 'venue_id']);
  });
};
