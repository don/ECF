exports.up = function(knex, Promise) {
  return knex.schema.table('users', function (table) {
    table.string('school');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.table('users', function (table) {
    table.dropColumn('school');
  });
};
