exports.up = function(knex, Promise) {
  return knex.schema.createTable('booths', function (table) {
    table.increments();
    table.integer('exhibitor_id').unsigned()
    table.foreign('exhibitor_id').references("exhibitors.id").onDelete('cascade');
    table.integer('venue_id').unsigned()
    table.foreign('venue_id').references("venues.id").onDelete('cascade');
    table.specificType('location', 'box')
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('booths');
};
;
