
exports.up = function(knex, Promise) {
  return knex.schema.table('booths', function (table) {

    table.string('booth_number');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.table('booths', function (table) {
    table.dropColumn('booth_number')
 });
};