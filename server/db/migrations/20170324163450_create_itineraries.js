
exports.up = function(knex, Promise) {
  return knex.schema.createTable('itineraries', function (table) {
    table.increments();
    table.integer('exhibitor_id').unsigned();
    table.foreign('exhibitor_id').references("exhibitors.id").onDelete('cascade');
    table.integer('user_id').unsigned();
    table.foreign('user_id').references("users.id").onDelete('cascade');

  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('itineraries');
};
