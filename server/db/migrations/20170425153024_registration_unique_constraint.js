
exports.up = function(knex, Promise) {
  return knex.schema.alterTable('registrations', function(table) {
    table.unique(['user_id', 'venue_id']);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.alterTable('registrations', function(table) {
    table.dropUnique(['user_id', 'venue_id']);
  });
};
