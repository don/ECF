
exports.up = function(knex, Promise) {
  return knex.schema.table('itineraries', function(table) {
    table.integer('venue_id').references("venues.id").onDelete('cascade');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.table('itineraries', function(table) {
    table.dropColumn('venue_id');
  });
};
