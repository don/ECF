const MAILGUN_API_KEY = process.env.MAILGUN_API_KEY;
const mailgun = require('mailgun-js')({apiKey: MAILGUN_API_KEY, domain: 'sandboxbf4ec2410d594c33b097b4f5d2901d11.mailgun.org'});
const ejs = require('ejs');
const path = require('path');
const qrImage = require('qr-image');
var fs = require('fs');

function file(name) {
    return fs.createWriteStream(__dirname + '/' + name);
}

const user= {user: "Zuzka"}
// const qr = qrCode.qrcode(4, 'L');
// qr.addData(user.user);
// qr.make();
// // console.log(qr.createImgTag(4));
// user.qr = qr.createImgTag(4);

async function string(user) {
  const email = await ejs.renderFile(path.join(__dirname, '../templates/confirmation.ejs'), user, function(err, str) {
      return str;
    })
  return email;
}

async function getData(user){

  const loadedMail = await string(user);
  let data = {
    from: 'ECF Registration <me@samples.mailgun.org>',
    to: 'zuzana.toldyova@gmail.com',
    subject: 'Confirmation of your registration',
    html: loadedMail,
    // attachment: attch
  };
  return data;
}

module.exports = {

  async sendMailCreated(data) {
    try{
      const dataForQr = `First name: ${data.first_name}, Last Name: ${data.last_name}, email: ${data.email}`;
      const emailData = await getData(data);
      qrImage.image(dataForQr, { type: 'png', ec_level: 'M'}).pipe(file('qr.png')).on('close', (err) => {
        const fp = path.join(__dirname, '/qr.png');
        let fileStat = fs.statSync(fp);
        filesize = fileStat.size;
        var fileStream = fs.readFileSync(fp);
        let attch = new mailgun.Attachment({
          data: fileStream,
          filename: 'qr.png',
          knownLength: filesize,
          contentType: 'image/png'
        });
        console.log(attch);
        emailData.inline = attch;
        mailgun.messages().send(emailData, function (error, body) {
          console.log("mail sent: ", body);
        });
      })
    } catch (err) {
      console.log(err);
    }
  }
};