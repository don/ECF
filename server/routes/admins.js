"use strict";

const express = require('express');
const router  = express.Router();

module.exports = (queries) => {

  router.get("/",  (req, res) => {
    queries.getAdminInfo().then((data) => {
      res.send(data);
    });
  });

  return router;
};