 "use strict";

const express = require('express');
const router  = express.Router();
const bcrypt = require('bcrypt');
const mail = require('../lib/email');

module.exports = (queries, util) => {

  router.get("/", (req, res) => {

    const templateVars = {
          userId        : 1,
          restaurantId  : 1,
          menuItems     : 1,
          totals        : 1
    };

    res.json(templateVars);
  });

  // POST METHODS
  // User Registration
  router.post("/", (req, res) => {
    bcrypt.hash(req.body.password, 10)
    .then(hashedPassword => {
      return {
        first_name: req.body.first,
        last_name: req.body.last,
        email: req.body.email,
        school: req.body.school,
        grade: Number(req.body.grade),
        password: hashedPassword,
        admin: false
      };
    })
    .then(data => {
      return queries.insertUser(data)
        .then(([id]) => {
          req.session.userId = id;
          return id;
        });
    })
    .then(user_id => {
      let regData = {
        user_id,
        venue_id: req.query.q
      };
      return queries.insertRegistration(regData);
    })
    .then(() => {
      res.status(200).json({message: "User saves"});
      mail.sendMailCreated({
        first_name: req.body.first,
        last_name: req.body.last,
        email: req.body.email
      });
    })
    .catch(err => {
      let msg = util.formatMessage(err);
      res.status(500).json({message: msg});
    });
  });

  router.post("/logout", (req, res) => {
    req.session = null;
    res.send({result: true});
  });

  router.post("/login", (req, res) => {
    let data = {
      email: req.body.email,
      admin: req.body.admin
    };
    let user;
    queries.getUser(data)
    .then(rows => {
      user = rows[0];
      if (!user) {
        return Promise.reject({message: 'Invalid email/password'});
      }
      return bcrypt.compare(req.body.password, user.password);
    })
    .then(passwordMatch => {
      if (!passwordMatch){
        return Promise.reject({message: 'Invalid email/password'});
      }
      return Promise.resolve(user);
    })
    .then(user => {
      req.session.userId = user.id;
      res.json({result: true});
    })
    .catch(err => {
      console.log(err);
      res.status(500).json(err);
    });
  });

  router.get("/info", (req, res) => {
    queries.findUserInfo(req.session.userId)
    .then(user => {
      res.json(user[0]);
    })
    .catch(err => {
      res.json(err);
    });
  });

  // route for creating a registration of already created user for a specific venue
  router.post('/register', (req, res) => {
    const user_id = req.session.userId;
    const venue_id = req.body.venueId;
    queries.insertRegistration({user_id, venue_id})
    .then(() => {
      res.json({message: "Registered"});
    })
    .catch(err => {
      res.json(err);
    });
  });

  router.get('/registrations', (req, res) => {
    queries.getRegistrations(req.session.userId)
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      res.json(err);
    });
  });

  return router;
};
