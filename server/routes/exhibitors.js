"use strict";

const express = require('express');
const router  = express.Router();

module.exports = (queries, util) => {

  router.get("/", (req, res) => {
    const venueId = req.query.venueId;
    if (venueId) {
      queries.getVenueExhibitors(venueId, (result) => {
        res.json(result);
      });

    } else {
      queries.getAllExhibitors()
      .then((data) => {
        res.json(data);
      })
      .catch(err => {
        res.status(500).json(err);
      });
    }
  });

  router.get("/search", util.checkLogin,  (req, res) => {
    const tagPartial = req.query.keyword;
    const venueId = req.query.venueId;
    queries.getExhibitorByTags(venueId, tagPartial, (result) => {
      res.send(result);
    });
  });

  router.post("/", (req, res) => {
    const venues = req.body.venues;
    const tags = req.body.tags;
    const data = Object.keys(req.body).reduce((previous, key) => {
      if(key !== 'venues' && key !== 'tags') {
        previous[key] = req.body[key];
      }
      return previous;
    }, {});
    queries.insertExhibitors(data)
    .then(([id]) => {
      if (!id) {
        return Promise.reject({message: "Exhibitor couldn't be inserted, please contact your system administrator."});
      }
      let data = [];
      venues.forEach(venue => {
        data.push({venue_id: venue, exhibitor_id: id});
      });
      return queries.insertExhibitorsToVenue(data);
    })
    .then(([id]) => {
      if (!id) {
        return Promise.reject({message: "Exhibitor couldn't be inserted, please contact your system administrator."});
      }
      let exhibitorId = id;
      let data = [];
      tags.forEach(tag => {
        data.push({tag_id: tag, exhibitor_id: exhibitorId});
      });
      return queries.insertExhibitorTags(data);
    })
    .then(([id]) => {
      if(!id) {
        return Promise.reject({message: "Exhibitor tags couldn't be inserted, please contact your system administrator."})
      }
      res.json(id);
    })
    .catch(err => {
      res.status(500).json(err);
    });
  });

  router.get('/:id', (req, res) => {
    const exhibitorId = req.params.id;
    let data;
    queries.getExhibitor(exhibitorId)
    .then(rows => {
      data = rows;
      if (!data) {
        return Promise.reject({message: "Couldn't find exhibitor."});
      }
      return queries.getExhibitorTags(exhibitorId);
    })
    .then(tags => {
      if(!tags) {
        return Promise.reject({message: "Couldn't find exhibitor."});
      }
      let tagIds = tags.map(tag => tag.tag_id);
      data.map(item => {
        item.tags = tagIds;
      });
      return Promise.resolve(data);
    })
    .then(data => {
      res.json(data);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json(err);
    });
  });

  router.put('/:id', (req, res) => {
    const exhibitorId = req.params.id;
    const venues = req.body.venues;
    const tags = req.body.tags;
    const data = Object.keys(req.body).reduce((previous, key) => {
      if(key !== 'venues' && key !== 'tags') {
        previous[key] = req.body[key];
      }
      return previous;
    }, {});
    queries.updateExhibitor(data, exhibitorId)
    .then((result) => {
      if (!result) {
        return Promise.reject({message: "Exhibitor couldn't be updated, please contact your system administrator."});
      }
      return queries.deleteAllBooths(exhibitorId);
    })
    .then((result) => {
      if (!result) {
        return Promise.reject({message: "Exhibitor couldn't be updated, please contact your system administrator."});
      }
      let data = [];
      venues.forEach(venue => {
        data.push({venue_id: venue, exhibitor_id: exhibitorId});
      });
      return queries.insertExhibitorsToVenue(data);
    })
    .then((result) => {
      if (!result) {
        return Promise.reject({message: "Exhibitor couldn't be updated, please contact your system administrator."});
      }
      return queries.deleteAllExhibitorsTags(exhibitorId);
    })
    .then((result) => {
      if(!result) {
        return Promise.reject({message: "Exhibitor couldn't be updated."});
      }
      let data = [];
      tags.forEach(tag => {
        data.push({tag_id: tag, exhibitor_id: exhibitorId});
      });
      return queries.insertExhibitorTags(data);
    })
    .then((result) => {
      if(!result) {
        return Promise.reject({message: "Exhibitor couldn't be updated."});
      }
      res.send({message: 'Updated'});
    })
    .catch(err => {
      res.status(500).json(err);
    });
  });

  router.delete('/:id', (req, res) => {
    const exhibitorId = req.params.id;
    queries.deleteExhibitor(exhibitorId)
    .then((result) => {
      if (!result) {
        return Promise.reject({message: "Exhibitor couldn't be deleted, please contact your system administrator."});
      }
      res.send('Deleted');
    })
    .catch(err => {
      res.status(500).json(err);
    });
  });

  return router;

};
