
import React, {Component} from 'react';
import { Table } from 'react-bootstrap';
import VenueInfo from './VenueInfo';


class Statistics extends Component {

  render() {
    return (

    <Table striped bordered condensed hover >
      <thead>
        <tr>
          <th>City</th>
          <th>Registrants</th>
          <th>Exhibitors</th>
        </tr>
      </thead>

      <tbody>

      {this.props.Statistics.map((stt) => {
        return (
          <VenueInfo
            key={stt.id}
            VenueId={stt.id}
            venueName={stt.name}
            venueReg={stt.Reg}
            VenueExh={stt.exh}
            clickOnVenue={this.props.clickOnVenue}
          />
        );
      })}

      </tbody>

    </Table>

    );
  }
}

export default Statistics;