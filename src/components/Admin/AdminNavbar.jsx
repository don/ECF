import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

class AdminNavbar extends Component {

  render() {
    return(
      <header>
        <Navbar inverse collapseOnSelect className="admin-nav">
          <Navbar.Header>
            <Navbar.Brand>
              <a href="/">ECF</a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav pullRight>
              <NavItem eventKey={1}>Logout</NavItem>
              <NavItem onClick={this.props.openForm}>Add Exhibitor</NavItem>
              <NavItem onClick={this.props.openTagsForm}>Add Tags</NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
      );
  }
}

export default AdminNavbar;