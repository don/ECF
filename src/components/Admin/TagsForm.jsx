import React, { Component } from 'react';
import {Form, FormGroup, ControlLabel,
        FormControl, Button} from 'react-bootstrap';

class TagsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = (e) => {
    this.setState({tagsString: e.target.value});
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const data = this.convertInput();
    this.props.submit({tags: data});
    this.props.close();
  }

  convertInput() {
    const tagsArray = this.state.tagsString.split(',');
    const trimmed = tagsArray.map(tag => {
      return {tag: tag.trim()};
    });
    return trimmed;
  }

  render(){
    return(
      <div>
        <Form horizontal id="" autoComplete="on" onSubmit={this.handleSubmit}>
          <FormGroup  controlId="tags" onChange={this.handleChange}>
            <ControlLabel>Tags</ControlLabel>
            <FormControl name="tag" type="text" required placeholder="Insert tags separated by comma"/>
            <FormControl.Feedback />
          </FormGroup>
          <Button onClick={this.handleSubmit}> Send </Button>
        </Form>
      </div>

    )
  }
}

export default TagsForm;