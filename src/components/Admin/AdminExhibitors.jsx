import React, {Component} from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

class AdminExhibitors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: ''
    }
  }

  handleDetails = (e) => {
    this.props.clickDetails(e);
  }

  addFilter(){
    return (e) => {
      this.setState({filter: e.target.value});
    }
  }

  render(){

    let data = this.props.exhibitors.filter(exhibitor => (exhibitor.venues.includes(this.props.venueId)) && exhibitor.name.match(new RegExp(this.state.filter, 'i')));

    const columns = [
    // {
    //   header: 'Id',
    //   accessor: 'exhibitor_id'
    // },
    {
      header: 'Name',
      accessor: 'name',
    }, {
      id: 'Workshop',
      header: 'Workshop',
      accessor: d => (d.workshop ? <i className="fa fa-check-square-o" /> : <i className="fa fa-square-o" />),
      maxWidth: 100,
      className: 'checkboxes',
    },{
      id: 'Seminar',
      header: 'Seminar',
      accessor: d => (d.seminar ? <i className="fa fa-check-square-o" /> : <i className="fa fa-square-o" />),
      maxWidth: 100,
      className: 'checkboxes',
    },{
      id: 'Sponsor',
      header: 'Sponsor',
      accessor: d => (d.sponsor ? <i className="fa fa-check-square-o" /> : <i className="fa fa-square-o" />),
      maxWidth: 100,
      className: 'checkboxes',
    },{
      id: 'Edit',
      header: 'Edit',
      accessor: d => (<button className="btn btn-primary" onClick={this.handleDetails} data-id={d.exhibitor_id}> Details </button> ),
      maxWidth: 125,
      className: 'checkboxes',
    }];

    return(

      <div>
        <div className="row">
          <h1>List of exhibitors for {this.props.venueName}</h1>
          <button className="btn btn-default btn-lg col-sm-offset-11" onClick={this.props.handleBack}> Back </button>
          <div className="form-group col-sm-2">
            <input type="text" className="form-control" placeholder="Search" onChange={this.addFilter()}/>
          </div>
        </div>
        <ReactTable
          data={data}
          columns={columns}
          defaultPageSize={10}
          loadingText={'Loading...'}
          className="-striped"
        />
      </div>
    )
  }
}

export default AdminExhibitors;