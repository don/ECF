import React, { Component } from 'react';
import { Alert, Button, Checkbox, Col, Form,
  FormGroup, FormControl, ControlLabel,
  HelpBlock, Row} from 'react-bootstrap';
import {validate} from '../utils/validations';

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first: {value: null, isValid: false},
      last: {value: null, isValid: false},
      email: {value: null, isValid: false},
      password: {value: null, isValid: false},
      agree: {value: null, isValid: false},
      firstValidation: null,
      lastValidation: null,
      emailValidation:null,
      passwordValidation: null,
      agreeValidation: null
    }
  }

  handleChange = (e) => {
    const input = e.target.name;
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    this.setState({
      [input]: { value: value, isValid: validate(e.target) }
    })
  }

  handleSubmit = (e) => {
    console.log('handleSumbit')
    e.preventDefault()
    let data = {};
    const arrData = ['first','last', 'email', 'password', 'agree'];
    let valid = true;
    arrData.forEach(name => {
      if (name !== 'agree') {
        data[name] = this.state[name].value;
      }
      if (!this.state[name].isValid){
        valid = false;
        this.setState({
          [name+'Validation']: 'error'
        })
      } else {
        this.setState({
          [name+'Validation']: 'success'
        })
      }
    });
    if (valid){
      this.props.submit(data)
    }
  }

  render() {

    return(

        <Col className="registering" xsPull={0} xs={12} smPush={7} sm={5}>
         <Col className=" reg-inner-colomn " sm={8} smOffset={0} >

          <Row className="reg-title">
            <h2 className="reg"> Registration </h2>
          </Row>
          <Form horizontal id="userReg" autoComplete="on" onSubmit={this.handleSubmit}>

            <FormGroup  controlId="formHorizontalFirstName" validationState={this.state.firstValidation} onChange={this.handleChange} >
              <Col className=" reg-item " xsOffset={2} xs={8} smOffset={0} sm={12} mdOffset={3} md={6} >
                <ControlLabel className="form-label"></ControlLabel>
                <FormControl className="form-input" name="first" type="text" placeholder="&#xf2c0;   First Name" />
                <FormControl.Feedback />
                {(this.state.firstValidation === 'error') ? <HelpBlock> Name must contain just letters </HelpBlock> :  null }
              </Col>
            </FormGroup>


            <FormGroup name="last" controlId="formHorizontalLastName" validationState={this.state.lastValidation} onChange={this.handleChange}>
              <Col className=" reg-item " xsOffset={2} xs={8} smOffset={3} sm={6} mdOffset={3} md={6} >

                <ControlLabel className="form-label"></ControlLabel>
                <FormControl className="form-input" name="last" type="text" placeholder="&#xf2c0;   Last Name" />
                <FormControl.Feedback />
                {(this.state.lastValidation === 'error') ? <HelpBlock> Name must contain just letters </HelpBlock> : null}
              </Col>
            </FormGroup>

            <FormGroup name="email" controlId="formHorizontalEmail" validationState={this.state.emailValidation} onChange={this.handleChange}>
              <Col className="reg-item " xsOffset={2} xs={8} smOffset={3} sm={6} mdOffset={3} md={6} >

                <ControlLabel className="form-label"></ControlLabel>
                <FormControl className="form-input" name="email" type="email" placeholder="&#xf003;   Email" />
                <FormControl.Feedback />
                {(this.state.emailValidation === 'error') ? <HelpBlock> Email must be this format: ex@ex.com </HelpBlock> : null}
              </Col>
            </FormGroup>

            <FormGroup  controlId="formHorizontalPassword" validationState={this.state.passwordValidation} onChange={this.handleChange}>
              <Col className="reg-item" xsOffset={2} xs={8} smOffset={3} sm={6} mdOffset={3} md={6} >
                <ControlLabel className="form-label"></ControlLabel>
                <FormControl.Feedback />
                <FormControl className="form-input" name="password" type="password" placeholder="&#xf023;    *******" />
                {(this.state.passwordValidation === 'error') ? <HelpBlock> Minimum length 6 characters </HelpBlock> : null}
              </Col>
            </FormGroup>

            <FormGroup validationState={this.state.agreeValidation} onChange={this.handleChange}>
              <Checkbox className="form-label" name="agree">I agree with Terms & Conditions</Checkbox>
              {(this.state.agreeValidation === 'error') ? <HelpBlock> You must agree with Terms </HelpBlock> : null}

            </FormGroup>

            <FormGroup>
              <Col className="rebutton-margin" xsOffset={4} xs={8} smOffset={4} sm={8} mdOffset={4} md={6}>
                <Button className="regButton" type="submit">
                  Sign Up
                </Button>
              </Col>
            </FormGroup>
          </Form>
          {(this.props.message) ?
            <Col className="" xsOffset={3} xs={6}>
            <Alert bsStyle="danger" > {this.props.message}
            </Alert> </Col> : null
          }
         </Col>
        </Col>

      );
  }
}
export default Registration;


