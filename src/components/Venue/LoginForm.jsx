import React, { Component } from 'react';
import { Alert, Button, ControlLabel, Form, FormControl, FormGroup, Modal } from 'react-bootstrap';
class LoginForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null
    }
  }

  get close () {
    return e => {
      this.props.close()
    }
  }

  get handleChange() {
    return e => {
      const name = e.target.name;
      const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
      this.setState({
        [name]: value
      })
    }
  }

  handleSubmit = (e) => {
      e.preventDefault();
      let data = {
        email: this.state.email,
        password: this.state.password,
        admin: this.props.admin
      }
      this.props.login(data)
  }


  render() {
    return(
        <Modal className="login-modal" show={this.props.show} onHide={this.close}>
          <Modal.Header closeButton className="login-container" >
            <Modal.Title id="login-modal-title">Login</Modal.Title>
          </Modal.Header >
          <Modal.Body className="login-container" >
            <Form className="login-container" horizontal id="userlog" autoComplete="on" onSubmit={this.handleSubmit}>

              <FormGroup   onChange={this.handleChange}>
                <ControlLabel className="form-label"> <span className="glyphicon glyphicon-envelope"></span> Email</ControlLabel>
                <FormControl className="form-input" id="login-form-border" name="email" type="email" placeholder="Enter your email"/>
                <FormControl.Feedback />
              </FormGroup>

              <FormGroup   onChange={this.handleChange}>
            <ControlLabel className="form-label"> <span className="glyphicon glyphicon-lock"></span> Password </ControlLabel>

                <FormControl className="form-input" id="login-form-border" name="password" type="password" placeholder="*******"/>
                <FormControl.Feedback />
              </FormGroup>

              <Button id="logButton" type="submit">
                  Login
              </Button>
              {(this.props.message) ?
              <Alert className='erroLogin'bsStyle="danger" > {this.props.message}
              </Alert>  : null
              }
            </Form>
          </Modal.Body>
        </Modal>
      );
  }

}

export default LoginForm;