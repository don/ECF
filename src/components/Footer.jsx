import React, {Component} from 'react';

class Footer extends Component {

  render(){
    return(
      <footer>
        <div>
          <p className="footer-info"> <i className="fa fa-envelope-o" aria-hidden="true"></i>  Questions? <a href="mailto:info@educationcareerfairs.com">info@educationcareerfairs.com</a>
          </p>
          <span className="footer-social">
            <i className="fa fa-twitter-square" aria-hidden="true"></i>
            <i className="fa fa-facebook-square" aria-hidden="true"></i>
          </span>
        </div>
      </footer>

      );
  }
}

export default Footer;