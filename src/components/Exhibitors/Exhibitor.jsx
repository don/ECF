import React, {Component} from 'react';

class Exhibitor extends Component {

  constructor(props) {
    super(props);
    this.handleClick.bind(this);
  }

  handleClick = () => {
    window.open(this.props.exhibitorData.url_link);
  }

  render(){

    return(
      <div className="exhibitor col-sm-2 col-xs-6" onClick={this.handleClick}>
        <h3> {this.props.exhibitorData.name} </h3>
        <img alt="" src="http://fillmurray.com/200/200" />
        <p> {this.props.exhibitorData.description} </p>
      </div>
    )
  }

}

export default Exhibitor;