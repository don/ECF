import React, {Component} from 'react';
import Exhibitor from './Exhibitor';

class ExhibitorsContainer extends Component {

  render() {

    const exhibitors = this.props.exhibitors.map((exhibitor) => {
      return <Exhibitor key={exhibitor.exhibitor_id} exhibitorData={exhibitor} />
    });

    return(
      <div className="exh-container">
        {exhibitors}
      </div>
    )
  }

}

export default ExhibitorsContainer;