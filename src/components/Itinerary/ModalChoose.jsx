import React, {Component} from 'react';
import {Button, FormGroup, Modal} from 'react-bootstrap';

class ModalChoose extends Component {
  constructor(props){
    super(props);
  }

  handleClick = (e) => {
    this.props.redirect(e);
  }

  render(){
    let venues = this.props.venues ? this.props.venues.map((venue) => {
      return (
        <Button key={venue.id} id={venue.id} name={ venue.name } onClick={this.handleClick} >
          { venue.name }
        </Button>
      )
    }) : null;


    return (
      <Modal show={this.props.show} onHide={this.close}>
        <Modal.Header closeButton>
          Select which venue would you like to see your itinerary:
        </Modal.Header>
        <Modal.Body>
          <FormGroup>
            { venues }
          </FormGroup>
        </Modal.Body>
      </Modal>
      )
  }
}

export default ModalChoose;