import React, {Component} from 'react';
import { ListGroup } from 'react-bootstrap';

import UserItineraryItems from './UserItineraryItems';


class UserItineraryContainer extends Component {

  render() {

    return (

      <ListGroup className="over-flowing">
          {this.props.itinerary.map((iti,index) => {
            return (
             <UserItineraryItems
              key={iti.id}
              {...iti}
             />
            );
          })}
      </ListGroup>

    );
  }
}

export default UserItineraryContainer;
