import React, {Component} from 'react';
import {Button, FormGroup, Modal} from 'react-bootstrap';

class Register extends Component{
  constructor(props) {
    super(props);

    this.state = {};
  }

  get close () {
    return e => {
      this.props.close()
    }
  }

  handleClick = (e) => {
    this.setState({confirmation: true, venueName: e.target.name, venueId: e.target.id})
  }

  handleRegister = (e) => {
    e.preventDefault();
    this.props.register(this.state.venueId);
  }

  render(){
    let venues = this.props.venues ? this.props.venues.map((venue) => {
      return (
        <Button key={venue.id} id={venue.id} name={ venue.name } onClick={this.handleClick} >
          { venue.name }
        </Button>
      )
    }) : null;

    return(

      <Modal show={this.props.show} onHide={this.close}>
        <Modal.Header closeButton>
        Pick up a venue to register
        </Modal.Header>
        <Modal.Body>
          <FormGroup>
            { venues }
          </FormGroup>
          { this.state.confirmation &&
          <div>
            <p>
              Register for venue {this.state.venueName}
            </p>
            <Button onClick={this.handleRegister}>
              Confirm
            </Button>
          </div>}
        </Modal.Body>
      </Modal>
    )
  }
}

export default Register;