import React from 'react';
import Workshop from './Workshop';
import {ListGroup} from 'react-bootstrap';

const WorkshopSeminarContainer  = (props) => (

  <ListGroup>
    {
      props.workshopsSeminars.map((workshop) => (
        <Workshop key={workshop.id} { ...workshop } onPick={props.onPick} />
      ))
    }
  </ListGroup>

);



export default WorkshopSeminarContainer;