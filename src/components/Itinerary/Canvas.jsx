
import React, {Component, PropTypes } from 'react';
import { withRouter } from 'react-router';

class CanvasWrapper extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props);
    console.log('before',props)

    this.state = {
      venueMap: props.venueMap,
      venueId: props.match.params.venueId,
      //venueIncoming: props.pickeddata
    }
  }
// method to find the pixel location on canvas
  saveCanvasRef(el) {
    this.canvas = el;

    if(!this.canvas) { return; }

    this.canvas.addEventListener('click', e => {
      const { offsetX, offsetY } = e;

      console.log('Canvas Clicked', offsetX, offsetY);
    })
  }


  renderCanvas(props) {
    const canvasBaseWidth = 940;
    const canvasBaseHeight = 500;
    const { venueId, venueMap } = this.state;

    if(this.canvas && venueId) {
      console.log('Updating canvas')
      const win_w = window.innerWidth; //this.state['windowWidth'];
      const win_h = window.innerHeight; //this.state['windowHeight'];

      // console.log("cw before", this.canvas.width);
      // console.log("ww/wh:", window.innerWidth, window.innerHeight);

      let scale_factor = Math.min(1, (0.75 * win_w) / canvasBaseWidth) || 1;   // maybe setState

      // let scale_nima_factor =

      // console.log("Jeremy lives in fear of debuggers")
      // console.log("t.c.w:", this.canvas.width);
      // console.log("win_w", win_w);
      // console.log("(0.75 * win_w)", (0.75 * win_w));
      // console.log("")
      // console.log("M.min", Math.min(1, (0.75 * win_w) / this.canvas.width));
      // console.log("sf:", scale_factor);

      this.canvas.width = canvasBaseWidth * scale_factor;
      this.canvas.height = canvasBaseHeight * scale_factor;
      // console.log("cw after", this.canvas.width);


      const ctx = this.canvas.getContext('2d');
      const src = `/maps/${venueId}.jpg`;

      const img = new Image();

      img.onload = () => {
        const { width, height } = img;

        if(width > height) {
          // landscape
          ctx.drawImage(img, 0, 0, canvasBaseWidth * scale_factor, canvasBaseHeight * scale_factor);
        } else {
          // portrait
          ctx.drawImage(img, 0, 0, canvasBaseWidth * scale_factor, canvasBaseHeight * scale_factor);
        }
        //console.log('Ctor', this.state.venueIncoming);

        this.props.pickeddata.forEach(x =>{

            if(x.location){
           //console.log('here we are',(x.location.split(/\D+/).filter(x=> x !== '').map(x=>+x)))

            ctx.save();


            ctx.fillStyle = "rgba(25, 215, 55, 1)";
            ctx.strokeStyle = "#ff0000";

            let location = (x.location.split(/\D+/).filter(x=> x !== '').map(x=>+x));
            // ctx.strokeRect(location[0],location[1],location[2],location[3]);
            ctx.fillRect(
              location[0] * scale_factor,
              location[1] * scale_factor,
              location[2] * scale_factor,
              location[3] * scale_factor
            );

            ctx.restore();


          }
        });

        // venueMap.forEach(([x1, y1, x2, y2, stroke, fill]) => {
        //   ctx.save();

        //   ctx.fillStyle = fill;
        //   ctx.strokeStyle = stroke;

        //   ctx.fillRect(0.5 + x1, 0.5 + y1,x2,y2);
        //   ctx.strokeRect(x1,y1,x2,y2);

        //   ctx.restore();
        // });
      };

      img.onerror = () => {
        console.error('Yolo Swag');
      };

      img.src = src;
    }
  }

  componentDidMount() {
    console.log('componentDidMount', this.state, this.props);
    this.renderCanvas(this.props);
    window.addEventListener("resize", this.handleResize.bind(this));
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps', nextProps.pickeddata);
    const ctx = this.canvas.getContext('2d');
    ctx.clearRect(0,0,940,500);

    this.renderCanvas(nextProps);

  }

  handleResize (){
    this.setState({
      windowHeight: window.innerHeight,
      windowWidth: window.innerWidth,
    })
    this.renderCanvas(this.props);
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
    window.removeEventListener("resize", this.handleResize.bind(this));
  }


   render() {
    return (
      <div>
        <canvas  ref={ this.saveCanvasRef.bind(this) } width="940" height="500" />
      </div>
    );
  }

}

export default withRouter(CanvasWrapper);

