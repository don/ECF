import React, {Component} from 'react';

class Pick extends Component {

  constructor(props) {
    super(props);
    this.handleClick.bind(this);
  }

  handleClick = () => {
    this.props.onDelete({
      name: this.props.name,
      id: this.props.id,
      workshop: this.props.workshop,
      seminar: this.props.seminar,
    });
  }

  render(){

    return(
      <li className="list-group-item list-align">
        {this.props.name}
        <i className="fa fa-trash-o fa-fw" aria-hidden="true" onClick={this.handleClick}></i>
      </li>
    )
  }
}

export default Pick;