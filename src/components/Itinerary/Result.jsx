import React, {Component} from 'react';

class Result extends Component {

  constructor(props) {
    super(props);
    this.handleClick.bind(this);
  }

  handleClick = (e) => {
    this.props.onPick({
      name: this.props.name,
      id: this.props.id,
      location: this.props.boothLocation
    });
  }

  render(){
    return(
      <li className="list-align list-group-item adding" onClick={this.handleClick}>
        {this.props.name}
        <i className="fa fa-plus-square-o fa-fw" aria-hidden="true"></i>
      </li>
    )
  }

}

export default Result;