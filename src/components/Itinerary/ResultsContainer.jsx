import React  from 'react';
import Result from './Result';
import {ListGroup} from 'react-bootstrap';

const ResultsContainer = (props) => (

  <ListGroup>
  {
    props.results.map((result) => (
      <Result key={result.id} { ...result } onPick={props.onPick} />
    ))
  }
  </ListGroup>
);

export default ResultsContainer;
