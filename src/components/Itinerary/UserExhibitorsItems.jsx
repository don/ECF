import React, {Component} from 'react';

class UserExhibitorsItems extends Component {

  constructor(props) {
    super(props);
    this.handleClick.bind(this);
  }

  handleClick = () => {
    this.props.onpick({name: this.props.name, id: this.props.id, location: this.props.location});
  }

  render() {

    return (
      <li className="list-group-item list-align adding" onClick={this.handleClick}>
        {this.props.name}
        <i className="fa fa-plus-square-o fa-fw" aria-hidden="true"></i>
      </li>
    );
  }
}

export default UserExhibitorsItems;
