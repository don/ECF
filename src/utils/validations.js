export function validate(data) {
  switch (data.type) {
    case 'text':
      const regex = /[a-zA-Z]+/g;
      let matches = data.value.match(regex);
      if (data.name === 'school'){
        if (data.value === null){
          matches = true;
        }
      }
      if (matches){
        return true;
      }
      return false;
    case 'select-one':
      if (data.value !== 'select' || null){
        return true;
      }
      return false;
    case 'email':
      const regEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/g
      let matchEmail = data.value.match(regEmail)
      if (matchEmail){
        return true;
      }
      return false;
    case 'password':
      const reg = /^[0-9a-zA-Z\-_.]{6,}$/g;
      let matchesP = data.value.match(reg);
      if (matchesP){
        return true;
      }
      return false;
    case 'checkbox':
      if (data.checked) {
        return true;
      }
      return false
    default:
      return null;
  }
}
