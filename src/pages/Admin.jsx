import React, {Component} from 'react';
import {Alert} from 'react-bootstrap';
import AdminExhibitors from '../components/Admin/AdminExhibitors';
import AdminNavbar from '../components/Admin/AdminNavbar';
import Statistics from '../components/Admin//Statistics';
import AddExhibitorForm from '../components/Admin/AddExhibitorForm';
import VenueStatistics from '../components/Admin/VenueStatistics';
import { get, post, put } from '../utils/fetch';

class DataError extends Error {}

class Admin extends Component{

  constructor(props) {
    super(props);
    this.state = {
      exhibitors: [],
      venueId: '',
      venueName: '',
      statistics: [],
      showModal: false,
      loginModal: true,
      loginMessage: null,
      message: null,
      tags: [],
    }
    this.clickOnVenue = this.clickOnVenue.bind(this);
  }

  componentDidMount() {
    this.getStats();
    this.getVenues();
    this.getExhibitors();
    this.getTags();
  }

  async getTags() {
    try {
      const data = await get('/tags')
      this.setState({tags: data});
    } catch(ex) {
      this.setState({error: ex.message});
    }
  }

  getVenues() {
    return get('/venues')
    .then((data) => {
      this.setState({venues: data});
    });
  }

  getExhibitors(id){
    if (id) {
      return get(`/exhibitors?venueId=${id}`)
       .then(data => {
          this.setState({
            exhibitors: this.formatExhibitors(data)
        });
      });
    } else {
      return get(`/exhibitors`)
       .then(data => {
          this.setState({
            exhibitors: this.formatExhibitors(data)
        });
      });
    }
  }

  // formatting data from server: instead of more exhibitors with the same id and different venue ids create one exhibitor
  // with an array of venues which he belongs to
  formatExhibitors(data) {
    let formattedExhibitors = []
    data.forEach(exhibitor => {
      let index = formattedExhibitors.map(ex => { return ex.exhibitor_id;}).indexOf(exhibitor.exhibitor_id);
      if (index === -1) {
        let formatExhibitor = Object.keys(exhibitor).reduce((previous, key) => {
          if (key !== 'venue_id') {
            previous[key] = exhibitor[key];
          }
          return previous;
        }, {});
        formatExhibitor.venues = [exhibitor.venue_id];
        formattedExhibitors.push(formatExhibitor);
      } else {
        formattedExhibitors[index].venues.push(exhibitor.venue_id);
      }
    });
   return formattedExhibitors;
  }

  // handles clicking on details button, sets state of current exhibitor to autofill the form data
  clickDetails = (e) => {
    return get(`/exhibitors/${e.target.dataset.id}`)
    .then((data) => {
      this.setState({ currentExhibitor: data });
      this.openModal()
    });
  }

  // statistics for chart
  getStats() {
    return get('/admins')
    .then((data) => {
     this.setState({statistics: data});
    });
  }

  // when clicked on venue, sets state venueId to satisfy condition for a specific venue view
  clickOnVenue(id, name){
    this.setState({venueId: id, venueName: name});
  }

  // back button from specific venue view to general "first page" view, clears state of venueId
  handleBack = () => {
    this.setState({venueId: ''});
  }

  // closing modal used for opening addExhibitorForm and tagsForm
  closeModal = () => {
    this.setState({
      showModal: false,
      currentExhibitor: '',
      tagsForm: false
    });
  }

  // opening modal
  openModal = () => {
    this.setState({showModal: true });
  }

  // opening modal with tagsForm
  openTagsForm = () => {
    this.setState({showModal: true, tagsForm: true});
  }

  // updates state of a page on front end when exhibitor is updated or created without calling a database
  updateStateExhibitor(exhibitor) {
    let newExhibitors = this.state.exhibitors;
    let newExhibitor = exhibitor;
    // if currentExhibitor is set it means that we are updating an existing exhibitor, otherwise the new exhibitor is added to the state
    if (this.state.currentExhibitor){
      newExhibitor.exhibitor_id = this.state.currentExhibitor[0].exhibitor_id;
      let index = newExhibitors.map(ex => { return ex.exhibitor_id }).indexOf(newExhibitor.exhibitor_id);
      newExhibitors[index] = newExhibitor;
    } else {
      newExhibitors = this.state.exhibitors.concat(newExhibitor);
    }
    this.setState({exhibitors: newExhibitors});
  }

  // deletes exhibitor from the state on front end
  deleteStateExhibitor() {
    let index = this.state.exhibitors.map(ex => { return ex.exhibitor_id }).indexOf(this.state.currentExhibitor[0].exhibitor_id);
    this.state.exhibitors.splice(index, 1);
  }


  // Functions for adding, editing and deleting exhibitors below
  get addExhibitor() {
    return async (data) => {
      this.closeModal();
      try {
        const id = await post(`/exhibitors`, data);
        //waiting for exhibitor id from database so that the state can be properly updated
        data.exhibitor_id = id;
        this.updateStateExhibitor(data);
      }
      catch(ex) {
        this.setState({ error: ex.message });
        this.getExhibitors(0);
      }
    }
  }

  updateExhibitor = async (data) => {
    this.updateStateExhibitor(data);
    this.closeModal();
    let modified = Object.keys(data).reduce((previous, key) => {
      if(key !== 'exhibitor_id') {
        previous[key] = data[key];
      }
      return previous;
    }, {});
    try {
      await put(`/exhibitors/${this.state.currentExhibitor[0].exhibitor_id}`, modified);
      this.setState({oldExhibitors: null});
    }
    catch(ex) {
      this.setState({ error: ex.message });
      this.getExhibitors(0);
    }
  }

  deleteExhibitor = async () => {
    this.deleteStateExhibitor();
    this.closeModal();
    try {
      const response = await fetch(`/exhibitors/${this.state.currentExhibitor[0].exhibitor_id}`, {
        method: 'DELETE',
        credentials: "include"
      });
      if(!response.ok) {
        const data = await response.json();
        console.log('error', data);
        throw new DataError(data.message);
      }
      this.setState({oldExhibitors: null})
      return response.json();
    }
    catch(ex) {
      this.setState({ error: ex.message });
      this.getExhibitors(0);
    }
  }

  // Functions for adding tags
  addTag = async (data) => {
    try {
      const response = await post('/tags', data);
      this.setState({tags: this.state.tags.concat(response.inserted)});
    }
    catch(ex) {
      this.setState({error: ex.message});
    }
  }

  // functions handling admin login
  adminSession(){
    return true;
  }

  async handleLogin(data) {
    try {
      let response = await post(`/users/login`, data);
      if (response.result) {
        this.setState({
          loginModal: false
        });
        this.closeModal();
      }
    }
    catch(ex) {
      this.setState({
        loginMessage: ex.message
      })
    }
  }

  render(){
    // popping up login modal at the beginning of the session
    // if(this.state.loginModal) {
    //   return <LoginForm show={this.openModal} close={this.closeModal} login={this.handleLogin.bind(this)} message={this.state.loginMessage} admin={this.adminSession()} />
    // }
    return(
      <div className="admin-section">
        <AdminNavbar openForm={this.openModal} openTagsForm={this.openTagsForm}/>
        {(this.state.error) &&
          <Alert bsStyle="danger"> {this.state.error}
          </Alert>
        }
        {this.state.venueId ? (
          <div  className="main-container">
            <AdminExhibitors
              handleBack={this.handleBack}
              venueName={this.state.venueName}
              venueId={this.state.venueId}
              exhibitors={this.state.exhibitors}
              clickDetails={this.clickDetails}
            />
            <VenueStatistics venueId={this.state.venueId} />
          </div>
        ) : (
          <div className="main-container">
            <div className="container container-table">
              <div className="row text-center">
                <div className="col-sm-12" >
                  <Statistics Statistics={this.state.statistics} clickOnVenue={this.clickOnVenue}/>
                </div>
              </div>

            </div>
          </div>
        )}
        {this.state.showModal &&
          <div>
            <AddExhibitorForm
              tags={this.state.tagsForm}
              addTag={this.addTag}
              venues={this.state.venues}
              editExhibitor={this.updateExhibitor}
              deleteExhibitor={this.deleteExhibitor}
              addExhibitor={this.addExhibitor}
              closeModal={this.closeModal}
              currentExhibitor={this.state.currentExhibitor}
              allTags={this.state.tags}
            />
          </div>
        }

      </div>
    )
  }
}

export default Admin;
