import React, {Component} from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import ResultsContainer from '../components/Itinerary/ResultsContainer'
import PickedContainer from '../components/Itinerary/PickedContainer';
import UserItineraryContainer from '../components/Itinerary/UserItineraryContainer';
import UserExhibitorsContainer from '../components/Itinerary/UserExhibitorsContainer';
import WorkshopSeminarContainer from '../components/Itinerary/WorkshopSeminarContainer';
import Navbarcomp from '../components/NavbarComp';
import Canvas from '../components/Itinerary/Canvas';
import Footer from '../components/Footer';
import MyQR from '../components/Itinerary/QRcode.jsx';
import Register from '../components/Itinerary/Register';
import ModalChoose from '../components/Itinerary/ModalChoose';
import { get , post  } from '../utils/fetch';


class Itinerary extends Component {

  constructor(props) {
    super(props);
    this.state = {
      // value is for autocomplete select, don't change name
      value: [],
      resultsExhibitors: [],
      picked: [],
      pickedSeminarsAndWorkshops: [],
      messages: [],
      workshops: [],
      submitState: false,
      user: "",
      // remembers ids of searched items to prevent sending search for a tag for one user multiple times into database
      searched: [],
      showFlashMessage : false,
      showErrorMessage: false,
      flashmessage: "",
      exhibitors:[],
      venueMap: [
        [175, 145, 30, 30, "#000000", "#ff0000"],
        [539, 143, 567 - 539, 162 - 143, "#00ff00", "#ffffff"]
      ],
      showModal: null,
      showModalReg: null,
      showModalSelect: true,
      userRegistrations: null
    }

    this.hideMessage=this.hideMessage.bind(this);
  }

  componentWillMount(){
    this.getVenues();
    this.getVenueName();
  }

  componentWillReceiveProps(nextProps) {
    this.getVenueName(nextProps);
  }

  componentDidMount() {
    this.getExhibitors();
    this.getItinerary();
    this.userInfoForQR();
    this.getUserTags();
    this.getUserRegistration();
    this.getVenueName(this.props);
  }

  getVenueName = async(props) => {
    try {
      const data = await get(`/venues/${props.match.params.venueId}`);
      this.setState({venueName: data.venue_name});
    } catch(ex) {
      this.setState({error: ex.message});
    }
  }
  // get name of the venue for headers

  // updateItinerary(){
  //   this.getExhibitors();
  //   this.getItinerary();
  //   this.getUserTags();
  //   this.getUserRegistration();
  // }

  async getVenues() {
    try {
      const data = await get('/venues');
      const venues = await get('/users/registrations');
      let registrations = [];
      for (let i = 0; i < data.length; i++){
        for (let j = 0; j < venues.length; j++ ){
          if (data[i].id === venues[j].venue_id){
            registrations.push({id: data[i].id, name: data[i].name});
          }
        }
      }
      this.setState({venues: data, userRegistrations: registrations});
    } catch (ex) {
      console.log(ex);
    }
  }

  // helper function returning Id of venue user is currently on
  get venueId() {
    return this.props.match.params.venueId;
  }

  // initial getting of exhibitors to show in the exhibitorsContainer in the final view
  getExhibitors() {
    return get(`/exhibitors?venueId=${this.venueId}`)
     .then(data => {
        const exhibitors = this.handleExhibitors(data);
        this.setState({
          exhibitors
        });
    });
  }

  // initial getting of user itinerary
  getItinerary() {
    return get(`/venues/${this.venueId}/itinerary`)
    .then((data) => {
      data.forEach(item => {
        if (item.workshop || item.seminar){
          this.setState({pickedSeminarsAndWorkshops: this.state.pickedSeminarsAndWorkshops.concat(item)})
        } else {
          this.setState({picked: this.state.picked.concat(item)});
        }
      })
      if (this.state.picked.length){
        this.setState({submitState: true});
      }
    });
  }

  // getting tags user searched for already to check if the searched tag is new and should be posted to the database
  getUserTags = async() => {
    try {
      const data = await get('/tags/searches');
      if (data.tags.length) {
        this.setState({
          searched: data.tags.map(tag => {
            return tag.value
          })
        })
      }
    } catch(ex) {
      this.setState({error: ex.message});
    }
  }

  getUserRegistration = async() => {
    try {
      const registrations = await get('/users/registrations');
      if (registrations.length) {
        const venuesRegistered = registrations.map(registration => registration.venue_id);
        if (venuesRegistered.includes(Number(this.venueId))) {
          this.setState({
            registered: true
          });
        }
      }
    } catch(ex) {
      this.setState({error: ex.message});
    }
  }

  userInfoForQR(){
    return get(`/users/info`)
    .then((data) => {
      this.setState({
        user: `First name: ${data.first_name}, Last Name: ${data.last_name}, email: ${data.email}`
      });
    });
  }

  // loading tags options for autocomplete
  getTags(input) {
    return fetch(`/venues/:id/tags?keyword=${input}`, {
      credentials: 'include'
    })
    .then((response) => { return response.json(); })
    .then((data) => { return {options: data };  });
  }

  // getting all exhibitors and workshops for selected tag
  getResults(value) {
    // to clear results before adding results for a new tag
    this.setState({
      resultsExhibitors: [],
      workshops: []
    });
    return fetch(`/exhibitors/search?keyword=${value.label}&venueId=${this.venueId}`,{
      credentials: 'include'
    })
    .then( response => { return response.json(); })
    .then((data) => {
      const handled = this.handleExhibitors(data);
      handled.forEach(item => {
        if (item.workshop || item.seminar){
          this.setState({workshops: this.state.workshops.concat(item)})
        } else {
          this.setState({resultsExhibitors: this.state.resultsExhibitors.concat(item)});
        }
      })
    });
  }

  // reduces data from all exhibitor information to information necassary for the front end function
  handleExhibitors(data) {
    let exhibitors = [];
    for (let exh of data){
      exhibitors.push({
        name: exh.name,
        id: exh.exhibitor_id,
        boothLocation: exh.boothLocation,
        workshop: exh.workshop,
        seminar: exh.seminar
      });
    }
    return exhibitors;
  }

  // posting a tag to the server to store search in a db
  sendTag(tagId) {
    fetch(`/venues/${this.venueId}/searches`, {
      method: 'POST',
      credentials: 'include',
      headers: {'content-type': 'application/json'},
      body: JSON.stringify({
        tagId
      })
    });
  }

  // on change of search bar determines if the tag was already searched for, if yes then sends it to the database and
  // shows result for last tag in the list of current tags (state of value)
  onChange = (value) => {
    if (value.length) {
      const lastItem = value[value.length - 1];
      if (!this.state.searched.includes(lastItem.value)) {
        this.sendTag(lastItem.value);
      }
      this.setState({
        searched: this.state.searched.concat(lastItem.value)
      });

      this.getResults(lastItem);
    }
    this.setState({value});
  }

  // handling picking of workshop, seminar or exhibitor into itinerary, checks if it's not there yet and then adds
  // to specific array (picked for exhibitors and pickedSeminarsAndWorkshops for seminars or workshop)
  handlePick() {
    return (exhibitor) => {
      if (this.state.picked.filter(e => e.id === exhibitor.id).length ||this.state.pickedSeminarsAndWorkshops.filter(e => e.id === exhibitor.id).length) {
        this.setState({
          messages: ['Exhibitor already on your list'],
          showErrorMessage : true,
        });
        this.hideMessage();
      } else {
        this.sendExhibitor(exhibitor);
        if (exhibitor.seminar || exhibitor.workshop) {
          this.setState({
            pickedSeminarsAndWorkshops: this.state.pickedSeminarsAndWorkshops.concat(exhibitor)
          });
        } else {
          this.setState({
            picked: this.state.picked.concat(exhibitor)
          });
        }
        this.setState({
          messages: [],
          flashmessage: exhibitor.name,
          showFlashMessage : true,
        });
        this.hideMessage();
      }
    }
  }

  // handles deleting exhibitor from picked items
  handleDelete() {
    return (exhibitor) => {
      this.deleteExhibitor(exhibitor);
      if (exhibitor.workshop || exhibitor.seminar) {
        let index = this.state.pickedSeminarsAndWorkshops.map(x => {return x.id}).indexOf(exhibitor.id);
        this.setState(this.state.pickedSeminarsAndWorkshops.splice(index, 1));
      } else {
        let index = this.state.picked.map(x => {return x.id}).indexOf(exhibitor.id);
        this.setState(this.state.picked.splice(index, 1));
      }
    }
  }

  // updating itinerary table after user picks an exhibitor into his itinerary
  sendExhibitor(data) {
    return fetch(`/venues/${this.props.match.params.venueId}/itinerary`, {
      method: 'POST',
      credentials: 'include',
      headers: {'content-type': 'application/json'},
      body: JSON.stringify({
        itinerary: data
      })
    });
  }

  // delete exhibitor from itinerary on the back end
  deleteExhibitor(exhibitor) {
    return fetch(`/venues/${this.venueId}/itinerary/delete`, {
      method: 'POST',
      credentials: 'include',
      headers: {'content-type': 'application/json'},
      body: JSON.stringify({
        itinerary: exhibitor
      })
    })
    .then(response => { return response.json(); })
    .then(data => {})
    .catch(err => {this.setState({err: err.message})});
  }

  // helper function for hiding flash messages
  hideMessage() {
    setTimeout(() => {
      this.setState({
        flashmessage: '',
        showFlashMessage : false,
        showErrorMessage: false
      });
    }, 2000);
  }

  // helper funtion for "redirecting" on the front end
  goToUrl = (url) => {
    this.props.history.push(url);
  }

  // loging out the user
  logout = async() => {
    try {
      const response = await post('/users/logout');
      if(response.result) {
        this.goToUrl(`/venues/${this.venueId}`);
      }
    } catch(ex) {
      this.setState({error: ex.message});
    }
  }

  // functions for opening and closing modal with QR code
  open = (e) => {
    if (e.target.name === 'QR'){
      this.setState({
        showModal: true
      })
    } else {
      this.setState({
        showModalReg: true
      })
    }
  }

  close = (e) => {
    this.setState({
      showModal: false,
      showModalReg: false,
      showModalSelect: false
    })
  }

  redirectVenue = (e) => {
    this.setState({
      showModalSelect: false
    })
    const venueIdSel = e.target.id;
    this.props.history.push(`/venues/${venueIdSel}/itinerary`);
  }

  // functions changing the condition between create itinerary view and submitted itinerary (final page) view
  handleSubmit = () => {
    this.setState({submitState: true});
  }

  handleRegister = async(id) => {
    try {
      const response = await post('/users/register', {venueId: id});
      if (response) {
        this.close();
        this.goToUrl(`/venues/${id}/itinerary`);
        window.location.reload();
      }
    } catch(ex) {
      this.setState({error: ex.message});
    }
  }

  handleEdit = () => {
    this.setState({submitState: false});
  }

  getNotRegistered = () => {
    let notRegistered = this.state.venues.filter(venue => this.state.userRegistrations.map(registration => registration.id).indexOf(venue.id) < 0);
    return notRegistered;
  }

  render() {

    // popping up modal to give the option to choose which venue the user would like to see
    // if(this.state.showModalSelect) {
    //   return <ModalChoose show={this.state.showModalSelect} close={this.closeModal} venues={this.state.userRegistrations} redirect={this.redirectVenue}/>
    // }
    return (

      <div className="max-height itinerary-page" >
        <Navbarcomp className="navbar" venueId={this.venueId} goTo={this.goToUrl} logout={this.logout} open={this.open}/>
        <div className="overwrapping">

            {this.state.submitState ? (
            <div>
              <div className="row" >
                <div className="box-container col-sm-12 col-xs-12">
                  <h1 className="itinerary-title">
                    Exhibition details for {this.state.venueName}:
                  </h1>
                  <button className="user-qr" name='QR' onClick={this.open}>
                    <i className="fa fa-qrcode fa-lg" aria-hidden="true"></i>  See your QR
                  </button>
                </div>
                <div className="selected col-sm-5  col-xs-12 effect8">
                  <h2> Your itinerary </h2>
                  <button className="itinerary-edit" onClick={this.handleEdit}>
                    <i className="fa fa-pencil" aria-hidden="true"></i> Big changes
                  </button>
                  <UserItineraryContainer itinerary={this.state.picked.concat(this.state.pickedSeminarsAndWorkshops)} />
                    {this.state.showFlashMessage &&
                      <div className="alert alert-success" role="alert">
                        {this.state.flashmessage} has been added to your itinerary
                      </div>
                    }
                    {this.state.showErrorMessage &&
                      <div className="alert alert-warning" role="alert">
                        {this.state.messages}
                      </div>
                    }
                </div>
                <div className="selected col-sm-5  col-xs-12 effect8">
                  <h2> Full list of exhibitors </h2>
                  <UserExhibitorsContainer exhibitors={this.state.exhibitors} onpick={this.handlePick()}/>
                </div>
              </div>
              <div className="mapcontainer effect8">
                <div className="row">
                  <div className="col-sm-8 col-sm-offset-1 col-xs-12">
                    <Canvas venueMap={this.state.venueMap} pickeddata={this.state.picked.concat(this.state.pickedSeminarsAndWorkshops)} />
                  </div>
                </div>
              </div>
              <div className="row" >
                <MyQR show={this.state.showModal} close={this.close} user={this.state.user}/>
              </div>
              <div>
                <Register show={this.state.showModalReg} close={this.close} venues={this.getNotRegistered()} register={this.handleRegister}/>
              </div>
            </div>
            ) : (
            <div className="full-page">
              <div className="row">
                <div className="col-sm-12 col-xs-12">
                  <h1 className="text-center">
                    Create your custom itinerary for {this.state.venueName}:
                  </h1>
                </div>
                <div className="col-sm-12 col-xs-12">
                  <h2 className="text-center">
                    What kind of topics are you interested in?
                  </h2>
                </div>
                <div className="align col-sm-6 col-sm-offset-3 col-xs-12">
                  <Select.Async
                    multi={true}
                    autoload={false}
                    name="form-field-name"
                    loadOptions={this.getTags}
                    onChange={this.onChange}
                    value={this.state.value}
                    placeholder="e.g. college, art, medical"
                  />
                </div>
              </div>
              <div className="row ">
                <div className="box-container col-sm-12">
                  <div className="selected effect8 col-sm-5 col-xs-12">
                    <h2> List of Exhibitors </h2>
                    {!this.state.resultsExhibitors.length && <p className="Select-placeholder fake"> List of exhibitors based on your last inserted tag.</p>}
                    <ResultsContainer results={this.state.resultsExhibitors} onPick={this.handlePick()}/>
                    <h2> List of Workshops and Seminars </h2>
                    {!this.state.workshops.length && <p className="Select-placeholder fake"> List of workshops and seminar based on your last inserted tag.</p>}
                    <WorkshopSeminarContainer workshopsSeminars={this.state.workshops} onPick={this.handlePick()}/>
                    {this.state.showFlashMessage &&
                      <div className="alert alert-success" role="alert">
                        {this.state.flashmessage} has been added to your itinerary
                      </div>
                    }
                    {this.state.showErrorMessage &&
                      <div className="alert alert-warning" role="alert">
                        {this.state.messages}
                      </div>
                    }
                  </div>
                  <div className="selected effect8 col-sm-5 col-xs-12">
                    <h2> Your Choices for Exhibitors </h2>
                    {!this.state.picked.length && <p className="Select-placeholder fake"> List of your exhibitors.</p>}
                    <PickedContainer picked={this.state.picked} onDelete={this.handleDelete()}/>
                    <h2> Your Workshops and Seminars </h2>
                    {!this.state.pickedSeminarsAndWorkshops.length && <p className="Select-placeholder fake"> List of your workhops and seminars.</p>}
                    <PickedContainer picked={this.state.pickedSeminarsAndWorkshops} onDelete={this.handleDelete()}/>
                  </div>
                </div>
              </div>
              <div className="button-div">
                <button className="button btn-3 icon-arrow-right" onClick={this.handleSubmit}>
                   Next
                  <i className="fa fa-arrow-right" aria-hidden="true"></i>
                </button>
              </div>
              <div>
                <Register show={this.state.showModalReg} close={this.close} venues={this.state.venues} register={this.handleRegister}/>
              </div>
            </div>
            )}

          <div className="row"> </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Itinerary;
